# Compiled resources used at the University of Toronto Climate Lab

[![license](https://licensebuttons.net/l/zero/1.0/80x15.png)](https://creativecommons.org/publicdomain/zero/1.0/)

This list contains links to data and software that the University of Toronto Climate Lab scientists use in their research. We have compiled this list in the interest of sharing with the scientific community at large. This list is a "living document" and is likely to change frequently as we discover new resources. Check back often!

### Contributing

This list is maintained by members of the Climate Lab, but we welcome contributions from other members of the scientific community and the public at large. Please submit a [merge request](https://gitlab.com/ConorIA/claut-resources/merge_requests), or send an email to [conor.anderson@mail.utoronto.ca](mailto:conor.anderson@mail.utoronto.ca) if you would like to contribute. Please note that your contributions will be part of the public domain under the conditions of the [CC 0 1.0 license](https://creativecommons.org/publicdomain/zero/1.0/).


## Data

### Historical Climate Data

#### Point Data

##### North America

-   Canada:
    -   [Canadian Historical Climate Data](http://climate.weather.gc.ca/historical_data/search_historic_data_e.html): hourly, daily, and monthly datasets for Canadian weather stations dating as early as 1840 for Environment and Climate Change Canada.
    -   [Adjusted and Homogenized Canadian Climate Data](http://ec.gc.ca/dccha-ahccd/): Historical Canadian climate data that has been adjusted to address data-quality issues arising from historical changes in measurement instruments and in observing practice. 

##### South America

-   Peru
    -   [Peru Clima](http://www.peruclima.pe/?p=data-historica): Historical temperature and precipitation data from Peru dating from 1965

#### Gridded Data

##### Worldwide

-   [NOAA  Global Historical Climatology Network (GHCN)](https://www.ncdc.noaa.gov/temp-and-precip/ghcn-gridded-products/): Gridded air and sea surface temperature anomaly data dating from 1880. 

### Projected Climate Data

##### Worldwide

-   [Earth System Grid Federation (ESGF)](https://esgf-node.llnl.gov/projects/esgf-llnl/): Official source for gridded data from ACME, input4MIPs, CMIP3, CMIP5, and, soon, CMIP6.

### Other Useful Data

##### Worldwide

-   [NCAR-USAR Climate Data Guide](https://climatedataguide.ucar.edu/): A data discovery platform maintained by NCAR with 195 data sets of the Atmosphere, Ocean, Land and more.


## Software

### Visualization tools

-   [NASA Panoply](https://www.giss.nasa.gov/tools/panoply/): For viewing gridded climate data

### R packages

#### Packages we use:

-   [canadaHCD: Canadian Historical Climate Data](https://github.com/gavinsimpson/canadaHCD/) by [Gavin L. Simpson](https://github.com/gavinsimpson)

#### Packages we maintain:

-   [canadaHCDx: Additional functions for the canadaHCD package](https://gitlab.com/ConorIA/canadaHCDx/)
-   [claut: Functions from the University of Toronto Climate Lab](https://gitlab.com/ConorIA/claut/)
-   [senamhiR: A Collection of Functions to Obtain Peruvian Climate Data](https://gitlab.com/ConorIA/senamhiR/)

## Documentation

-   [R for Climate Science](http://conoria.gitlab.io/r4cs): a [WIP] manual for analyzing climate time series in R [(source code)](https://gitlab.com/ConorIA/r4cs/)
